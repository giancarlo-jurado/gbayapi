﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GBayApi.Domain.Interfaces.Services;
using GBayApi.Domain.Interfaces.Repositories;
using GBayApi.Domain.Services;
using GBayApi.Infrastructure.MongoDb;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;


namespace GBayApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddSignalR();
            services.AddMvc();

            services.AddSingleton<IAuctionService, AuctionService>();
            services.AddSingleton<IBidService, BidService>();
            services.AddSingleton<IAuctionRepository, AuctionRepository>();
            services.AddSingleton<IBidRepository, BidRepository>();


            services.Configure<MongoDbSettings>(Configuration.GetSection("Mongodb"));
            services.RegisterMongodbServices();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());

            app.UseSignalR(routes =>
            {
                routes.MapHub<AuctionHub>("/hubs/auction");
            });

            app.UseMvc();
        }
    }
}
