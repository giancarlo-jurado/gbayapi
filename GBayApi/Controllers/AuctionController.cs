﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace GBayApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Auction")]
    public class AuctionController : Controller
    {
        private readonly IAuctionService _auctionService;
        private IHubContext<AuctionHub> context;

        public AuctionController(IAuctionService auctionService, IHubContext<AuctionHub> hub)
        {
            _auctionService = auctionService;
            context = hub;
        }

        [HttpGet]
        public IActionResult CurrentAuctions()
        {
            return Ok(_auctionService.GetCurrentAuctions());
        }

        [HttpPost]
        public async Task<IActionResult> CreateAuction([FromBody] Item item)
        {            
            var auction = _auctionService.CreateAuction(item);
            await context.Clients.All.SendAsync("AuctionAdded", auction);
            return Ok(auction);
        }
    }
}