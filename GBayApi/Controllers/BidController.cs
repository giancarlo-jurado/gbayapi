﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace GBayApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Bid")]
    public class BidController : Controller
    {
        private readonly IBidService _bidService;
        private IHubContext<AuctionHub> context;

        public BidController(IBidService bidService, IHubContext<AuctionHub> hub)
        {
            _bidService = bidService;
            context = hub;
        }

        [HttpGet]
        public IActionResult GetAuctionBids(string auctionId)
        {
            return Ok(_bidService.GetAuctionBids(auctionId));
        }

        [HttpPost]
        public async Task<IActionResult> PlaceBid([FromBody]Bid bid)
        {
            var bidSuccessful = _bidService.PlaceBid(bid);

            if(bidSuccessful)
            {
                await context.Clients.All.SendAsync("BidPlaced", bid);
            }            
            return Ok(bidSuccessful);
        }
    }
}