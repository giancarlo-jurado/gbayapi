﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces;
using GBayApi.Domain.Interfaces.Services;
using Microsoft.AspNetCore.SignalR;


namespace GBayApi
{
    public class AuctionHub : Hub<IAuctionClient>
    {
        public async Task AddAuction(Auction auction) => await Clients.All.AddAuction(auction);
        public async Task PlaceBid(Bid bid) => await Clients.All.PlaceBid(bid);
    }
}
