﻿using GBayApi.Controllers;
using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace GBayApi.Tests
{
    [TestClass]
    public class BidTest
    {
        [TestMethod]
        public async Task PlaceBidShouldAddNewBid()
        {
            var mockRepository = new Mock<IBidService>();
            var bid = new Bid { Amount = 100, AuctionId = Guid.NewGuid().ToString(), UserId = "email@email.com" };            

            mockRepository.Setup(x => x.PlaceBid(bid)).Returns(true);

            var mockHub = new Mock<IHubContext<AuctionHub>>();
            var mockClients = new Mock<IHubClients>();
            var all = new Mock<IClientProxy>();

            mockHub.Setup(m => m.Clients).Returns(mockClients.Object);
            mockHub.Setup(m => m.Clients.All).Returns(all.Object);

            var controller = new BidController(mockRepository.Object, mockHub.Object);

            var response = await controller.PlaceBid(bid) as OkObjectResult;

            Assert.AreEqual(true, response.Value);
        }
    }
}