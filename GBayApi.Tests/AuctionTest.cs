﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GBayApi.Controllers;
using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GBayApi.Tests
{
    [TestClass]
    public class AuctionTest
    {
        [TestMethod]
        public async Task AuctionShouldCreateNewAuction()
        {
            var mockRepository = new Mock<IAuctionService>();            

            var item = new Item { Title = "My Title", Description = "My Description", Price = 100 };
            var auction = new Auction();
            auction.Item = item;
            auction.StartingPrice = item.Price;

            mockRepository.Setup(x => x.CreateAuction(item)).Returns(auction);

            var mockHub = new Mock<IHubContext<AuctionHub>>();
            var mockClients = new Mock<IHubClients>();
            var all = new Mock<IClientProxy>();

            mockHub.Setup(m => m.Clients).Returns(mockClients.Object);
            mockHub.Setup(m => m.Clients.All).Returns(all.Object);

            var controller = new AuctionController(mockRepository.Object, mockHub.Object);

            var response = await controller.CreateAuction(item) as OkObjectResult;

            Assert.AreEqual(auction, response.Value);

        }
    }
}