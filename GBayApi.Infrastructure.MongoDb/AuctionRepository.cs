﻿using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using GBayApi.Domain;

namespace GBayApi.Infrastructure.MongoDb
{
    public class AuctionRepository : IAuctionRepository
    {
        private readonly IMongoCollection<Auction> _auctions;

        public AuctionRepository(IMongoDatabase database)
        {
            _auctions = database.GetCollection<Auction>("auctions");
        }

        public Auction CreateAuction(Auction auction)
        {
            var result = _auctions.ReplaceOne(p => p.Id == auction.Id, auction, new UpdateOptions { IsUpsert = true });

            return auction;
        }

        public List<Auction> GetCurrentAuctions()
        {
            var auctions = _auctions.AsQueryable().Where(p => p.Status == Status.InProgress).ToList();

            return auctions;
        }

        public Auction GetAuction(string auctionId)
        {
            var auction = _auctions.FindAsync(p => p.Id == auctionId).Result.FirstOrDefault();

            return auction;
        }

        public Auction SaveAuction(Auction auction)
        {
            var result = _auctions.ReplaceOne(p => p.Id == auction.Id, auction, new UpdateOptions { IsUpsert = true });

            return auction;
        }
    }
}
