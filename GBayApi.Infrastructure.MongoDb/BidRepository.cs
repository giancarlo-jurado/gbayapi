﻿using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces.Repositories;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Infrastructure.MongoDb
{
    public class BidRepository : IBidRepository
    {
        private readonly IMongoCollection<Bid> _bids;
        private readonly IMongoCollection<Auction> _auctions;

        public BidRepository(IMongoDatabase database)
        {
            _bids = database.GetCollection<Bid>("bids");
            _auctions = database.GetCollection<Auction>("auctions");

        }

        public void PlaceBid(Bid bid)
        {
            var result = _bids.ReplaceOne(p => p.Id == bid.Id, bid, new UpdateOptions { IsUpsert = true });         
        }

        public List<Bid> GetAuctionBids(string auctionId)
        {
            var bids = _bids.AsQueryable().Where(p => p.AuctionId == auctionId).ToList();

            return bids;
        }
    }
}
