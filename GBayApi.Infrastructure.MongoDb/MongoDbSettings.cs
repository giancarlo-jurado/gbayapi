﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Infrastructure.MongoDb
{
    public class MongoDbSettings
    {
        public string Connectionstring { get; set; }
        public string Database { get; set; }
    }
}
