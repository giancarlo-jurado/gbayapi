﻿using GBayApi.Domain.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Domain.Interfaces.Repositories
{
    public interface IAuctionRepository
    {
        List<Auction> GetCurrentAuctions();
        Auction CreateAuction(Auction auction);
        Auction GetAuction(string auctionId);
        Auction SaveAuction(Auction auction);
    }
}
