﻿using GBayApi.Domain.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Domain.Interfaces.Repositories
{
    public interface IBidRepository
    {
        void PlaceBid(Bid bid);
        List<Bid> GetAuctionBids(string auctionId);
    }
}
