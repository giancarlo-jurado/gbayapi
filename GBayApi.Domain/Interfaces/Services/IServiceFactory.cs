﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Domain.Interfaces.Services
{
    public interface IServiceFactory <out T> where T : class
    {
        T Build();
    }
}
