﻿using GBayApi.Domain.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Domain.Interfaces.Services
{
    public interface IBidService
    {
        bool PlaceBid(Bid bid);
        List<Bid> GetAuctionBids(string auctionId);
    }
}
