﻿using GBayApi.Domain.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Domain.Interfaces.Services
{
    public interface IAuctionService
    {
        Auction CreateAuction(Item item);
        List<Auction> GetCurrentAuctions();
    }
}
