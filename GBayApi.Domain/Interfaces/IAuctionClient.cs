﻿using GBayApi.Domain.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GBayApi.Domain.Interfaces
{
    public interface IAuctionClient
    {
        Task AddAuction(Auction auction);
        Task PlaceBid(Bid bid);
    }
}
