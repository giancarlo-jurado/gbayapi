﻿using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces.Services;
using GBayApi.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Domain.Services
{
    public class BidService : IBidService
    {
        private readonly IBidRepository _bidRepository;
        private readonly IAuctionRepository _auctionRepository;

        public BidService(IBidRepository bidRepository, IAuctionRepository auctionRepository)
        {
            _bidRepository = bidRepository;
            _auctionRepository = auctionRepository;
        }

        public bool PlaceBid(Bid bid)
        {
            var auction = _auctionRepository.GetAuction(bid.AuctionId);

            if (bid.Amount <= auction.HighestBid || bid.Amount <= auction.StartingPrice)
            {
                return false;
            }

            _bidRepository.PlaceBid(bid);
            
            auction.HighestBid = bid.Amount;
            auction.Bids++;

            _auctionRepository.SaveAuction(auction);            

            return true;
        }

        public List<Bid> GetAuctionBids(string auctionId)
        {
            return _bidRepository.GetAuctionBids(auctionId);
        }
    }
}
