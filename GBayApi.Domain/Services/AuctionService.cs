﻿using System;
using System.Collections.Generic;
using System.Text;
using GBayApi.Domain.Classes;
using GBayApi.Domain.Interfaces.Services;
using GBayApi.Domain.Interfaces.Repositories;

namespace GBayApi.Domain.Services
{
    public class AuctionService : IAuctionService
    {
        private readonly IAuctionRepository _auctionRepository;

        public AuctionService(IAuctionRepository auctionRepository)
        {
            _auctionRepository = auctionRepository;
        }

        public Auction CreateAuction(Item item)
        {
            var auction = new Auction();
            auction.StartingPrice = item.Price;
            auction.Item = item;

            var result = _auctionRepository.CreateAuction(auction);

            return auction;
        }

        public List<Auction> GetCurrentAuctions()
        {
            return _auctionRepository.GetCurrentAuctions();            
        }
    }
}
