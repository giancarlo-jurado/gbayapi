﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Domain
{
    public enum Status
    {
        InProgress,
        Finished,
        Cancelled
    }
}
