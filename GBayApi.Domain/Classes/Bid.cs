﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GBayApi.Domain.Classes
{
    public class Bid : Base
    {
        [Required]
        public string AuctionId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public string UserId { get; set; }

    }
}
