﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GBayApi.Domain.Classes
{
    public class Auction : Base
    {
        [Required]
        public string OwnerId { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public decimal StartingPrice { get; set; }
        public decimal HighestBid { get; set; }
        public int Bids { get; set; }
        public Status Status { get; set; }
        public Item Item { get; set; }

        public Auction()
        {
            Status = Status.InProgress;
        }

        
    }
}
