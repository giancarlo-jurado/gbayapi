﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GBayApi.Domain.Classes
{
    public class User : Base
    {        
        //UserName will be email address
        [EmailAddress]
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
