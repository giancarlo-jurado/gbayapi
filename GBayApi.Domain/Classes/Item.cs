﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GBayApi.Domain.Classes
{
    public class Item
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Decimal Price { get; set; }
    }
}
